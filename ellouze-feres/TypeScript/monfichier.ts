// Exemple de type complexe en TypeScript
type Utilisateur = {
    nom: string;
    age: number;
    email?: string; // Champ non obligatoire
    hobbies: string[]; // Un tableau de chaînes
    statut: "actif" | "inactif"; // Champ avec deux valeurs possibles
    };
    // Utilisation du type complexe
    const monUtilisateur: Utilisateur = {
    nom: "Alice",
    age: 30,
    hobbies: ["Lecture", "Voyage"],
    statut: "actif"
    };
    const autreUtilisateur: Utilisateur = {
    nom: "Bob",
    age: 25,
    email: "bob@example.com", // Champ facultatif
    hobbies: ["Musique"],
    statut: "inactif"
    };