// Un autre exemple avec un champ pouvant avoir plusieurs types
type Resultat = {
    valeur: number | string; // Le champ "valeur" peut être soit un nombre, soit une chaîne
    };
    const resultat1: Resultat = {
    valeur: 42 // Un nombre
    };
    const resultat2: Resultat = {
    valeur: "Succès" // Une chaîne
    };