// Utilisation du type complexe
var monUtilisateur = {
    nom: "Alice",
    age: 30,
    hobbies: ["Lecture", "Voyage"],
    statut: "actif"
};
var autreUtilisateur = {
    nom: "Bob",
    age: 25,
    email: "bob@example.com",
    hobbies: ["Musique"],
    statut: "inactif"
};
