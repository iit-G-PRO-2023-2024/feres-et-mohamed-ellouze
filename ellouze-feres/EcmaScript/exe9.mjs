import { add, substract } from './add.mjs'; 
import MathDiv from './add.mjs'; 

// Utilisation des fonctions importées
const resultatAddition = add(5, 3);
const resultatSoustraction = substract(10, 4);

console.log(`Résultat de l'addition : ${resultatAddition}`);
console.log(`Résultat de la soustraction : ${resultatSoustraction}`);