// TODO : Créez une chaîne de modèle qui affiche le nom et l'âge d'une personne.
const nom = "Alice";
const age = 25;
// TODO : Utilisez une chaîne de modèle pour afficher le nom et l'âge
const message = `Bonjour, je m'appelle ${nom} et j'ai ${age} ans.`;
// Test de la chaîne de modèle
console.log(message); // Attendu : "Bonjour, je m'appelle Alice et j'ai 25 ans."
