// TODO : Créez une classe "Personne" avec des propriétés telles que le nom, l'âge, etc. Ajoutez
// ensuite des méthodes pour obtenir et définir ces propriétés.
class Personne {
    constructor(nom, age) {
      this.nom = nom;
      this.age = age;
    }
  
    // Méthode pour obtenir le nom
    getNom() {
      return this.nom;
    }
  
    // Méthode pour définir le nom
    setNom(nouveauNom) {
      this.nom = nouveauNom;
    }
  
    // Méthode pour obtenir l'âge
    getAge() {
      return this.age;
    }
  
    // Méthode pour définir l'âge
    setAge(nouvelAge) {
      this.age = nouvelAge;
    }
  }
  
  const personne1 = new Personne("Alice", 25);
  
  // Tester les méthodes de la classe
  console.log("Nom de la personne 1 :", personne1.getNom());
  console.log("Âge de la personne 1 :", personne1.getAge());
  
  // Modifier le nom et l'âge de la personne
  personne1.setNom("Bob");
  personne1.setAge(30);
  
  console.log("Nouveau nom de la personne 1 :", personne1.getNom());
  console.log("Nouvel âge de la personne 1 :", personne1.getAge());
// TODO : Testez les méthodes de la classe