// TODO : Utilisez les littéraux d'objet améliorés pour créer un objet "utilisateur" avec des
//propriétés telles que le nom, l'adresse e-mail, etc.
// N'oubliez pas d'utiliser la nouvelle syntaxe d'objet améliorée.
const nom = "Alice";
const email = "alice@email.com";
const age = 30;

// Créez un objet "utilisateur" en utilisant les littéraux d'objet améliorés
const user = {
  nom, // Utilisation de la nouvelle syntaxe pour définir la propriété "nom"
  email, // Utilisation de la nouvelle syntaxe pour définir la propriété "email"
  age, // Utilisation de la nouvelle syntaxe pour définir la propriété "age"
};

// Test de l'objet utilisateur
console.log(user); 
console.log(user.nom); 
console.log(user.email); 
console.log(user.age); 



