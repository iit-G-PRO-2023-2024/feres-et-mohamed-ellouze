import React from 'react';
import { View } from 'react-native'; 
import { PromoCard } from '../../../component/PromoCard';
const PromoCardMeta = {
  title: 'Promo Card ',
  component: PromoCard,
  argTypes: {
    onPress: { action: 'pressed the button' },
  },
  args: {
    image: 'image.png',
    price: 1000,
    title: "Promo 1",
    promotion: 50
  },
  decorators: [
    (Story) => (
      <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
        <Story />
      </View>
    ),
  ],
};

export default PromoCardMeta;

//export const Basic = {};

export const Horizontal = {
  args: {
    orientation:'Horizontal',
    image: 'https://loremflickr.com/640/480',
    price: 10002,
    title: "Promo 1",
    promotion: 50
  },
};
 
