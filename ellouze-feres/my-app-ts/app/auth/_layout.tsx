import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { Slot } from 'expo-router'

const LayoutAuth = () => {
  return (
    <View>
      <Text>start LayoutAuth</Text>
      <Slot></Slot>
      <Text>end LayoutAuth</Text>
    </View>
  )
}

export default LayoutAuth

const styles = StyleSheet.create({})