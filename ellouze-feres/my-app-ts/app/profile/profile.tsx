import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Navtop from '../../component/NavTop'
import { useSelector } from 'react-redux'
import { Notification } from '../../component/Notification'
import Profile from '../../component/Profile'
import Divider from '../../component/Divider'
import Orders from '../../component/Orders'
import Ordersubmenu from '../../component/Ordersubmenu'
import Wishlist from '../../component/Wishlist'
import Reviews from '../../component/Reviews'
import Recentlyviewed from '../../component/Recentlyviewed'
import Accountsettings from '../../component/Accountsettings'
import Privacypolicy from '../../component/Privacypolicy'
import Helpcenter from '../../component/Helpcenter'
import About from '../../component/About'
import Buttonlogout from '../../component/Buttonlogout'
import Navbottom from '../../component/Navbottom'
import PromoButton from '../../component/PromoButton'
const profile = () => {
    const notifs = useSelector((state) => state.notification)

  return (
    <View>
      <Text></Text>
      <Navtop></Navtop>
      <Profile></Profile>
      <Divider></Divider>
      <Orders></Orders>
      <Ordersubmenu></Ordersubmenu>
      <Divider></Divider>
      <Wishlist></Wishlist>
      <Reviews></Reviews>
      <Recentlyviewed></Recentlyviewed>
      <Divider></Divider>
      <Accountsettings></Accountsettings>
      <Privacypolicy></Privacypolicy>
      <Helpcenter></Helpcenter>
      <About></About>
      <Buttonlogout></Buttonlogout>
      <Divider></Divider>
      <Navbottom></Navbottom>
      
      {/* {
        notifs?.notifications.map(notification => <Notification {...notification} icon={<Text>T</Text>}></Notification>)
      } */}
    </View>
  )
}

export default profile

const styles = StyleSheet.create({})


