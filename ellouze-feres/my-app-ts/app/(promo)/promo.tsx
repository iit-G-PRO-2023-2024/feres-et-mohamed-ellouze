import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect } from 'react'
 
import { service } from '../../service'
import { useDispatch, useSelector } from 'react-redux'
 
import { ScrollView } from 'react-native-gesture-handler'
import { themeGlobal } from '../../styles/themeGlobal'
import { PromoCard } from '../../component/PromoCard'
 


 

const Home = () => {
  const dispatch = useDispatch()
  const products = useSelector(state => state.product.products)

  useEffect( () =>{


    const fetchData = async() => {
        const {data} = await service.products.productsList({skip : 0 , take :  10})
        console.log(data.paginatedResult )
        dispatch({type : "SET_PRODUCTS", payload : data.paginatedResult })
    
    
    }
    fetchData()
  
  },[])


  return (
       
    <ScrollView>
     
     
      <Text style={themeGlobal.themeTextGlobal.h5}>Recommended for you</Text>
      <View style={{flexWrap: 'wrap' , flexDirection: "row", width:'100%'}}>
 
      {products.map(promo => <TouchableOpacity onPress={()=>console.log("click")} style={{width : '100%', alignItems:'center' , padding : 5}}><PromoCard orientation='Vertical' title={promo.title} price={promo.price} image={promo.image} promotion={promo.promotion} onClickFavoris={()=>console.log("test")}></PromoCard></TouchableOpacity>)}
      </View>
      
    
    </ScrollView>
  )
}

export default Home

const styles = StyleSheet.create({})
