import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect } from 'react'
 
import { ScrollView } from 'react-native-gesture-handler'
 import { useDispatch, useSelector } from 'react-redux'
import { service } from '../../service'
import { ImageCard } from '../../component/Image'
 

const Verticale  = () => {
  const dispatch = useDispatch()
  const products = useSelector(state => state.product.products)

  useEffect(async () =>{


    const {data} = await service.products.productsList({skip : 0 , take :  10})
    console.log(data.paginatedResult )
    dispatch({type : "SET_PRODUCTS", payload : data.paginatedResult })


  },[])
  return (
    <ScrollView> 
      
     
      {products.map(product => <TouchableOpacity onPress={()=>console.log("click")} style={{width : '116', height : '154', alignItems:'center' , padding : 5}}><ImageCard orientation='Vertical' title={product.title} image={product.image} onClickFavoris={()=>console.log("test")}></ImageCard></TouchableOpacity>)}
      </ScrollView>
       
  )
}

export default Verticale

const styles = StyleSheet.create({})