import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect } from 'react'
import { Link } from 'expo-router'
import { service } from '../../service'
import { useDispatch, useSelector } from 'react-redux'
 
import { ScrollView } from 'react-native-gesture-handler'
import { themeGlobal } from '../../styles/themeGlobal'
import { ImageCard } from '../../component/Image'
import { ProductCard } from '../../component/TemplateFinale'
import NavigationNavbar from '../NavBar/navigation'
 


 

const Home = () => {
  const dispatch = useDispatch()
  const products = useSelector(state => state.product.products)

  useEffect( () =>{

const fetchData = async() => {
  const {data} = await service.products.productsList({skip : 0 , take :  10})
    console.log(data.paginatedResult )
    dispatch({type : "SET_PRODUCTS", payload : data.paginatedResult })

}
  fetchData()

  },[])


  return (
       
    <ScrollView>
      <NavigationNavbar></NavigationNavbar>
      <Text style={themeGlobal.themeTextGlobal.h5}> Room ideas</Text>
      <ScrollView horizontal={true}  contentContainerStyle={{ flexDirection : 'row'}}>

      {products.map(product => <TouchableOpacity onPress={()=>console.log("click")} style={{width : '116', height : '154', alignItems:'center' , padding : 5}}><ImageCard orientation='Horizontal' title={product.title} image={product.image}  onClickFavoris={()=>console.log("test")}></ImageCard></TouchableOpacity>)}
      </ScrollView>
      <Text style={themeGlobal.themeTextGlobal.h5}> Shop by room</Text>
      <ScrollView horizontal={true}  contentContainerStyle={{ flexDirection : 'row'}}>

      {products.map(product => <TouchableOpacity onPress={()=>console.log("click")} style={{width : '116', height : '154', alignItems:'center' , padding : 5}}><ImageCard orientation='Vertical' title={product.title} image={product.image} onClickFavoris={()=>console.log("test")}></ImageCard></TouchableOpacity>)}
      </ScrollView>
      <Text style={themeGlobal.themeTextGlobal.h5}>Recommended for you</Text>
      <View style={{flexWrap: 'wrap' , flexDirection: "row", width:'100%'}}>

      {products.map(product => <TouchableOpacity onPress={()=>console.log("click")} style={{width : '50%', alignItems:'center' , padding : 5}}><ProductCard orientation='Vertical' title={product.title} price={product.price} image={product.image} promotion={product.promotion} onClickFavoris={()=>console.log("test")}></ProductCard></TouchableOpacity>)}
      </View>
      
      {/* <Link href={"notificationPage"}> notification</Link>
      <TouchableOpacity onPress={()=>{"notificationPage"}} style={{width : '116', height : '154', alignItems:'center' , padding : 5}}><ImageCard orientation='Horizontal' title={'testttt'} ></ImageCard></TouchableOpacity>) */}
    
    </ScrollView>
  )
}

export default Home

const styles = StyleSheet.create({})
