import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as React from 'react';
import { Text, View } from 'react-native';
import Home from '../(tabs)/home';
import Verticale from '../(tabs)/verticale';
import { NavigationContainer } from '@react-navigation/native';
const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Settings" component={Verticale} />
    </Tab.Navigator>
  );
}export default function Layout() {
    return (
      <NavigationContainer>
        <MyTabs />
      </NavigationContainer>

    );
  }