//export {default} from './.storybook'; 
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native'; 
import { useState } from 'react';
import FirstComponent from './component/FirstComponent';
import { ProductCard } from './component/TemplateFinale';
import { Notification } from './component/Notification';
import { ProductReview } from './component/ProductReview';
import { default as StoryBookDefault } from './.storybook'; 
import Constants from 'expo-constants';
import { ImageCard } from './component/Image';
import { Thumbnail } from './component/Thumbnail';
 
const  App = () => {


  const [products,] = useState([{
    image: 'image.png',
    price: 1000,
    title: "Product 1",
    promotion: 50
  } 
  ])

  const [images,] = useState([{
    image: 'image.png',
 
  } ])




  const [notifications,] = useState([
    {
    
    type: "type1",
    title: "title 1",
    subTitle: "subTitle 1"
  } 
  ])
  const [productReviews,] = useState([
    {
    
    user: "user1",
    color: "white",
    text: "TTTTTTTTTTTTTTTTT EEEEEEEEEEEEEe"
  } 
  ])


  const [thumbnails,] = useState([
    {

    image: ['image.png','image.png','image.png','image.png' ]
  } 
  ])
  const clickFavoris = (title: string) => {
    console.log(title)
    if (title == "Product 1") {
      console.log("ok ")
    }
  }

  return (

    <View style={styles.container}>
      <FirstComponent></FirstComponent>
  
      {
        thumbnails.map((thumbnail) =>
          <Thumbnail  {...thumbnail} ></Thumbnail>
        )
      }
  <Text>Vertical</Text>

      {
        products.map((product) =>
          <ProductCard orientation='Vertical' {...product} onClickFavoris={clickFavoris}></ProductCard>
        )
      }

{
        images.map((image) =>
          <ImageCard orientation='Vertical' {...image} ></ImageCard>
        )
      }



      {/* <ProductCard title='test' price='dddd'  ></ProductCard> */}


      {
        products.map((product) =>
          <ProductCard orientation='Vertical' {...product} onClickFavoris={clickFavoris}></ProductCard>
        )
      }

{
      notifications.map((notification) =>
        <Notification {...notification} ></Notification>
      )
 
}
{
      productReviews.map((productReview) =>
        <ProductReview {...productReview} ></ProductReview>
      )
 
}


      <StatusBar style="auto" />
    </View>
  );
}
let APP_FINAL =  null
APP_FINAL = App

if(Constants?.expoConfig?.extra?.storybookEnabled === "true")
{
   APP_FINAL = StoryBookDefault;
}

export default APP_FINAL



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
