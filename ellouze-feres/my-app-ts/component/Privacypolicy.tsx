import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Svg, Path } from 'react-native-svg';

export default function Privacypolicy() {
    return (
    		<View style={styles.privacypolicy}>
      			{/* Vigma RN:: can be replaced with <Iconleft  /> */}
      			<View style={styles.iconleft}>
        				<View style={styles.boundingbox}/>
<Svg style={styles.vector} width="14" height="19" viewBox="0 0 14 19" fill="none" >
<Path d="M2.00004 18.3333C1.54171 18.3333 1.14935 18.1701 0.822957 17.8437C0.496568 17.5174 0.333374 17.125 0.333374 16.6667V8.33333C0.333374 7.87499 0.496568 7.48263 0.822957 7.15624C1.14935 6.82986 1.54171 6.66666 2.00004 6.66666H2.83337V4.99999C2.83337 3.84722 3.23962 2.86458 4.05212 2.05208C4.86462 1.23958 5.84726 0.833328 7.00004 0.833328C8.15282 0.833328 9.13546 1.23958 9.94796 2.05208C10.7605 2.86458 11.1667 3.84722 11.1667 4.99999V6.66666H12C12.4584 6.66666 12.8507 6.82986 13.1771 7.15624C13.5035 7.48263 13.6667 7.87499 13.6667 8.33333V16.6667C13.6667 17.125 13.5035 17.5174 13.1771 17.8437C12.8507 18.1701 12.4584 18.3333 12 18.3333H2.00004ZM2.00004 16.6667H12V8.33333H2.00004V16.6667ZM7.00004 14.1667C7.45837 14.1667 7.85074 14.0035 8.17712 13.6771C8.50351 13.3507 8.66671 12.9583 8.66671 12.5C8.66671 12.0417 8.50351 11.6493 8.17712 11.3229C7.85074 10.9965 7.45837 10.8333 7.00004 10.8333C6.54171 10.8333 6.14935 10.9965 5.82296 11.3229C5.49657 11.6493 5.33337 12.0417 5.33337 12.5C5.33337 12.9583 5.49657 13.3507 5.82296 13.6771C6.14935 14.0035 6.54171 14.1667 7.00004 14.1667ZM4.50004 6.66666H9.50004V4.99999C9.50004 4.30555 9.25698 3.71527 8.77087 3.22916C8.28476 2.74305 7.69448 2.49999 7.00004 2.49999C6.3056 2.49999 5.71532 2.74305 5.22921 3.22916C4.7431 3.71527 4.50004 4.30555 4.50004 4.99999V6.66666Z" fill="#09111F"/>
</Svg>

      			</View>
      			<View style={styles.text}>
        				<Text style={styles.title}>
          					{`Privacy policy`}
        				</Text>
      			</View>
      			<View style={styles.itemright}>
        				{/* Vigma RN:: can be replaced with <Iconright  /> */}
        				<View style={styles.iconright}>
          					<View style={styles._boundingbox}/>
<Svg style={styles._vector} width="7" height="10" viewBox="0 0 7 10" fill="none" >
<Path d="M1.08329 9.41666C0.930515 9.26388 0.854126 9.06943 0.854126 8.83332C0.854126 8.59721 0.930515 8.40277 1.08329 8.24999L4.33329 4.99999L1.08329 1.74999C0.930515 1.59721 0.854126 1.40277 0.854126 1.16666C0.854126 0.930545 0.930515 0.736101 1.08329 0.583323C1.23607 0.430545 1.43051 0.354156 1.66663 0.354156C1.90274 0.354156 2.09718 0.430545 2.24996 0.583323L6.08329 4.41666C6.16663 4.49999 6.22565 4.59027 6.26038 4.68749C6.2951 4.78471 6.31246 4.88888 6.31246 4.99999C6.31246 5.1111 6.2951 5.21527 6.26038 5.31249C6.22565 5.40971 6.16663 5.49999 6.08329 5.58332L2.24996 9.41666C2.09718 9.56943 1.90274 9.64582 1.66663 9.64582C1.43051 9.64582 1.23607 9.56943 1.08329 9.41666Z" fill="#09111F"/>
</Svg>

        				</View>
      			</View>
    		</View>
    )
}

const styles = StyleSheet.create({
  	privacypolicy: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingLeft: 24,
    paddingRight: 20,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 8,
    paddingVertical: 10
},
  	iconleft: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	vector: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 3,
    bottom: 2,
    left: 3,
    overflow: "visible"
},
  	text: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 0
},
  	title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	itemright: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 0
},
  	iconright: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	_boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	_vector: {
    position: "absolute",
    flexShrink: 0,
    top: 5,
    right: 7,
    bottom: 5,
    left: 8,
    overflow: "visible"
}
})