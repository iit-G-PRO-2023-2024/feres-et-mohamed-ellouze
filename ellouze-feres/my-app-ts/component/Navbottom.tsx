import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Svg, Path, Circle } from 'react-native-svg';

export default function Navbottom() {
    return (
    		<View style={styles.navbottom}>
      			{/* Vigma RN:: can be replaced with <Item1 state={"default"} /> */}
      			<View style={styles.item1}>
        				{/* Vigma RN:: can be replaced with <Icon  /> */}
        				<View style={styles.icon}>
          					<View style={styles.boundingbox}/>
<Svg style={styles.vector} width="14" height="16" viewBox="0 0 14 16" fill="none" >
<Path d="M2.00004 13.8333H4.50004V8.83332H9.50004V13.8333H12V6.33332L7.00004 2.58332L2.00004 6.33332V13.8333ZM2.00004 15.5C1.54171 15.5 1.14935 15.3368 0.822957 15.0104C0.496568 14.684 0.333374 14.2917 0.333374 13.8333V6.33332C0.333374 6.06943 0.392402 5.81943 0.510457 5.58332C0.628513 5.34721 0.791707 5.15277 1.00004 4.99999L6.00004 1.24999C6.15282 1.13888 6.31254 1.05555 6.47921 0.99999C6.64587 0.944434 6.81949 0.916656 7.00004 0.916656C7.1806 0.916656 7.35421 0.944434 7.52087 0.99999C7.68754 1.05555 7.84726 1.13888 8.00004 1.24999L13 4.99999C13.2084 5.15277 13.3716 5.34721 13.4896 5.58332C13.6077 5.81943 13.6667 6.06943 13.6667 6.33332V13.8333C13.6667 14.2917 13.5035 14.684 13.1771 15.0104C12.8507 15.3368 12.4584 15.5 12 15.5H7.83337V10.5H6.16671V15.5H2.00004Z" fill="#09111F"/>
</Svg>

        				</View>
      			</View>
      			{/* Vigma RN:: can be replaced with <Item2 state={"default"} /> */}
      			<View style={styles.item2}>
        				{/* Vigma RN:: can be replaced with <_icon  /> */}
        				<View style={styles._icon}>
          					<View style={styles._boundingbox}/>
<Svg style={styles._vector} width="16" height="16" viewBox="0 0 16 16" fill="none" >
<Path d="M14 14.9167L9.33333 10.25C8.91667 10.5833 8.4375 10.8472 7.89583 11.0417C7.35417 11.2361 6.77778 11.3333 6.16667 11.3333C4.65278 11.3333 3.37153 10.809 2.32292 9.76042C1.27431 8.71181 0.75 7.43056 0.75 5.91667C0.75 4.40278 1.27431 3.12153 2.32292 2.07292C3.37153 1.02431 4.65278 0.5 6.16667 0.5C7.68055 0.5 8.96181 1.02431 10.0104 2.07292C11.059 3.12153 11.5833 4.40278 11.5833 5.91667C11.5833 6.52778 11.4861 7.10417 11.2917 7.64583C11.0972 8.1875 10.8333 8.66667 10.5 9.08333L15.1875 13.7708C15.3403 13.9236 15.4167 14.1111 15.4167 14.3333C15.4167 14.5556 15.3333 14.75 15.1667 14.9167C15.0139 15.0694 14.8194 15.1458 14.5833 15.1458C14.3472 15.1458 14.1528 15.0694 14 14.9167ZM6.16667 9.66667C7.20833 9.66667 8.09375 9.30208 8.82292 8.57292C9.55208 7.84375 9.91667 6.95833 9.91667 5.91667C9.91667 4.875 9.55208 3.98958 8.82292 3.26042C8.09375 2.53125 7.20833 2.16667 6.16667 2.16667C5.125 2.16667 4.23958 2.53125 3.51042 3.26042C2.78125 3.98958 2.41667 4.875 2.41667 5.91667C2.41667 6.95833 2.78125 7.84375 3.51042 8.57292C4.23958 9.30208 5.125 9.66667 6.16667 9.66667Z" fill="#09111F"/>
</Svg>

        				</View>
      			</View>
      			{/* Vigma RN:: can be replaced with <Item3 state={"default"} /> */}
      			<View style={styles.item3}>
        				{/* Vigma RN:: can be replaced with <IconsOutlineShopping_cart  /> */}
        				<View style={styles.iconsOutlineShopping_cart}>
          					<View style={styles.__boundingbox}/>
<Svg style={styles.__vector} width="17" height="18" viewBox="0 0 17 18" fill="none" >
<Path d="M5.33337 17.3333C4.87504 17.3333 4.48268 17.1701 4.15629 16.8437C3.8299 16.5174 3.66671 16.125 3.66671 15.6667C3.66671 15.2083 3.8299 14.816 4.15629 14.4896C4.48268 14.1632 4.87504 14 5.33337 14C5.79171 14 6.18407 14.1632 6.51046 14.4896C6.83685 14.816 7.00004 15.2083 7.00004 15.6667C7.00004 16.125 6.83685 16.5174 6.51046 16.8437C6.18407 17.1701 5.79171 17.3333 5.33337 17.3333ZM13.6667 17.3333C13.2084 17.3333 12.816 17.1701 12.4896 16.8437C12.1632 16.5174 12 16.125 12 15.6667C12 15.2083 12.1632 14.816 12.4896 14.4896C12.816 14.1632 13.2084 14 13.6667 14C14.125 14 14.5174 14.1632 14.8438 14.4896C15.1702 14.816 15.3334 15.2083 15.3334 15.6667C15.3334 16.125 15.1702 16.5174 14.8438 16.8437C14.5174 17.1701 14.125 17.3333 13.6667 17.3333ZM4.62504 3.99999L6.62504 8.16666H12.4584L14.75 3.99999H4.62504ZM5.33337 13.1667C4.70837 13.1667 4.23615 12.8924 3.91671 12.3437C3.59726 11.7951 3.58337 11.25 3.87504 10.7083L5.00004 8.66666L2.00004 2.33332H1.14587C0.909763 2.33332 0.715318 2.25346 0.562541 2.09374C0.409763 1.93402 0.333374 1.7361 0.333374 1.49999C0.333374 1.26388 0.413235 1.06596 0.572957 0.90624C0.73268 0.746518 0.930596 0.666656 1.16671 0.666656H2.52087C2.67365 0.666656 2.81949 0.708323 2.95837 0.791657C3.09726 0.87499 3.20143 0.993045 3.27087 1.14582L3.83337 2.33332H16.125C16.5 2.33332 16.757 2.47221 16.8959 2.74999C17.0348 3.02777 17.0278 3.31943 16.875 3.62499L13.9167 8.95832C13.7639 9.2361 13.5625 9.45138 13.3125 9.60416C13.0625 9.75694 12.7778 9.83332 12.4584 9.83332H6.25004L5.33337 11.5H14.5209C14.757 11.5 14.9514 11.5799 15.1042 11.7396C15.257 11.8993 15.3334 12.0972 15.3334 12.3333C15.3334 12.5694 15.2535 12.7674 15.0938 12.9271C14.9341 13.0868 14.7362 13.1667 14.5 13.1667H5.33337Z" fill="#09111F"/>
</Svg>

        				</View>
        				{/* Vigma RN:: can be replaced with <Badge type={"dot"} size={"small"} /> */}
        				<View style={styles.badge}>
<Svg style={styles.dot} width="7" height="6" viewBox="0 0 7 6" fill="none" >
<Circle cx="3.5" cy="3" r="3" fill="#E50D24"/>
</Svg>

        				</View>
      			</View>
      			{/* Vigma RN:: can be replaced with <Item4 state={"default"} /> */}
      			<View style={styles.item4}>
        				{/* Vigma RN:: can be replaced with <IconsOutlineFavorite  /> */}
        				<View style={styles.iconsOutlineFavorite}>
          					<View style={styles.___boundingbox}/>
<Svg style={styles.___vector} width="18" height="15" viewBox="0 0 18 15" fill="none" >
<Path d="M7.62496 14.3333L6.18746 13.0208C4.71524 11.6736 3.38538 10.3368 2.19788 9.01041C1.01038 7.68402 0.416626 6.22221 0.416626 4.62499C0.416626 3.31943 0.854126 2.22916 1.72913 1.35416C2.60413 0.479156 3.6944 0.0416565 4.99996 0.0416565C5.73607 0.0416565 6.43052 0.197906 7.08329 0.510407C7.73607 0.822907 8.29163 1.24999 8.74996 1.79166C9.20829 1.24999 9.76385 0.822907 10.4166 0.510407C11.0694 0.197906 11.7638 0.0416565 12.5 0.0416565C13.8055 0.0416565 14.8958 0.479156 15.7708 1.35416C16.6458 2.22916 17.0833 3.31943 17.0833 4.62499C17.0833 6.22221 16.493 7.68749 15.3125 9.02082C14.1319 10.3542 12.7916 11.6944 11.2916 13.0417L9.87496 14.3333C9.55552 14.6389 9.18052 14.7917 8.74996 14.7917C8.3194 14.7917 7.9444 14.6389 7.62496 14.3333ZM7.95829 3.45832C7.55552 2.88888 7.12496 2.45485 6.66663 2.15624C6.20829 1.85763 5.65274 1.70832 4.99996 1.70832C4.16663 1.70832 3.47218 1.9861 2.91663 2.54166C2.36107 3.09721 2.08329 3.79166 2.08329 4.62499C2.08329 5.34721 2.34024 6.11457 2.85413 6.92707C3.36802 7.73957 3.9826 8.52777 4.69788 9.29166C5.41315 10.0555 6.14927 10.7708 6.90621 11.4375C7.66315 12.1042 8.27774 12.6528 8.74996 13.0833C9.22218 12.6528 9.83677 12.1042 10.5937 11.4375C11.3507 10.7708 12.0868 10.0555 12.802 9.29166C13.5173 8.52777 14.1319 7.73957 14.6458 6.92707C15.1597 6.11457 15.4166 5.34721 15.4166 4.62499C15.4166 3.79166 15.1388 3.09721 14.5833 2.54166C14.0277 1.9861 13.3333 1.70832 12.5 1.70832C11.8472 1.70832 11.2916 1.85763 10.8333 2.15624C10.375 2.45485 9.9444 2.88888 9.54163 3.45832C9.4444 3.59721 9.32635 3.70138 9.18746 3.77082C9.04857 3.84027 8.90274 3.87499 8.74996 3.87499C8.59718 3.87499 8.45135 3.84027 8.31246 3.77082C8.17357 3.70138 8.05552 3.59721 7.95829 3.45832Z" fill="#09111F"/>
</Svg>

        				</View>
      			</View>
      			{/* Vigma RN:: can be replaced with <Item5 state={"active"} /> */}
      			<View style={styles.item5}>
        				{/* Vigma RN:: can be replaced with <IconsFilledPerson  /> */}
        				<View style={styles.iconsFilledPerson}>
          					<View style={styles.____boundingbox}/>
<Svg style={styles.____vector} width="14" height="14" viewBox="0 0 14 14" fill="none" >
<Path d="M7.00004 7.00001C6.08337 7.00001 5.29865 6.67362 4.64587 6.02084C3.9931 5.36807 3.66671 4.58334 3.66671 3.66668C3.66671 2.75001 3.9931 1.96529 4.64587 1.31251C5.29865 0.659732 6.08337 0.333344 7.00004 0.333344C7.91671 0.333344 8.70143 0.659732 9.35421 1.31251C10.007 1.96529 10.3334 2.75001 10.3334 3.66668C10.3334 4.58334 10.007 5.36807 9.35421 6.02084C8.70143 6.67362 7.91671 7.00001 7.00004 7.00001ZM2.00004 13.6667C1.54171 13.6667 1.14935 13.5035 0.822957 13.1771C0.496568 12.8507 0.333374 12.4583 0.333374 12V11.3333C0.333374 10.8611 0.454902 10.4271 0.697957 10.0313C0.941013 9.63543 1.26393 9.33334 1.66671 9.12501C2.52782 8.69445 3.40282 8.37154 4.29171 8.15626C5.1806 7.94098 6.08337 7.83334 7.00004 7.83334C7.91671 7.83334 8.81948 7.94098 9.70837 8.15626C10.5973 8.37154 11.4723 8.69445 12.3334 9.12501C12.7362 9.33334 13.0591 9.63543 13.3021 10.0313C13.5452 10.4271 13.6667 10.8611 13.6667 11.3333V12C13.6667 12.4583 13.5035 12.8507 13.1771 13.1771C12.8507 13.5035 12.4584 13.6667 12 13.6667H2.00004Z" fill="#09111F"/>
</Svg>

        				</View>
        				<Text style={styles.label}>
          					{`Profile`}
        				</Text>
      			</View>
    		</View>
    )
}

const styles = StyleSheet.create({
  	navbottom: {
    flexShrink: 0,
    width: 360,
    borderTopWidth: 1,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    borderLeftWidth: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    columnGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 24,
    borderColor: "rgba(225, 229, 235, 1)"
},
  	item1: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999
},
  	icon: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 3,
    bottom: 2,
    left: 3,
    overflow: "visible"
},
  	item2: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999
},
  	_icon: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	_boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	_vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 3,
    bottom: 3,
    left: 2,
    overflow: "visible"
},
  	item3: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999
},
  	iconsOutlineShopping_cart: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	__boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	__vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 3,
    bottom: 2,
    left: 1,
    overflow: "visible"
},
  	badge: {
    position: "absolute",
    flexShrink: 0,
    top: 7,
    height: 6,
    left: 27,
    width: 6,
    alignItems: "flex-start",
    rowGap: 0
},
  	dot: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    overflow: "visible"
},
  	item4: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999
},
  	iconsOutlineFavorite: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	___boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	___vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 2,
    bottom: 2,
    left: 2,
    overflow: "visible"
},
  	item5: {
    flexShrink: 0,
    paddingLeft: 10,
    paddingRight: 14,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    paddingVertical: 10,
    borderRadius: 9999
},
  	iconsFilledPerson: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	____boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	____vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 3,
    bottom: 3,
    left: 3,
    overflow: "visible"
},
  	label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
}
})