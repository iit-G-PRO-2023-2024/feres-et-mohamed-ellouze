import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Svg, Path } from 'react-native-svg';

export default function Wishlist() {
    return (
    		<View style={styles.wishlist}>
      			{/* Vigma RN:: can be replaced with <IconsOutlineFavorite  /> */}
      			<View style={styles.iconsOutlineFavorite}>
        				<View style={styles.boundingbox}/>
<Svg style={styles.vector} width="18" height="15" viewBox="0 0 18 15" fill="none" >
<Path d="M7.87496 14.3333L6.43746 13.0208C4.96524 11.6736 3.63538 10.3368 2.44788 9.01041C1.26038 7.68403 0.666626 6.22222 0.666626 4.625C0.666626 3.31944 1.10413 2.22916 1.97913 1.35416C2.85413 0.479164 3.9444 0.0416641 5.24996 0.0416641C5.98607 0.0416641 6.68052 0.197914 7.33329 0.510414C7.98607 0.822914 8.54163 1.25 8.99996 1.79166C9.45829 1.25 10.0138 0.822914 10.6666 0.510414C11.3194 0.197914 12.0138 0.0416641 12.75 0.0416641C14.0555 0.0416641 15.1458 0.479164 16.0208 1.35416C16.8958 2.22916 17.3333 3.31944 17.3333 4.625C17.3333 6.22222 16.743 7.6875 15.5625 9.02083C14.3819 10.3542 13.0416 11.6944 11.5416 13.0417L10.125 14.3333C9.80552 14.6389 9.43052 14.7917 8.99996 14.7917C8.5694 14.7917 8.1944 14.6389 7.87496 14.3333ZM8.20829 3.45833C7.80552 2.88889 7.37496 2.45486 6.91663 2.15625C6.45829 1.85764 5.90274 1.70833 5.24996 1.70833C4.41663 1.70833 3.72218 1.98611 3.16663 2.54166C2.61107 3.09722 2.33329 3.79166 2.33329 4.625C2.33329 5.34722 2.59024 6.11458 3.10413 6.92708C3.61802 7.73958 4.2326 8.52778 4.94788 9.29167C5.66315 10.0556 6.39927 10.7708 7.15621 11.4375C7.91315 12.1042 8.52774 12.6528 8.99996 13.0833C9.47218 12.6528 10.0868 12.1042 10.8437 11.4375C11.6007 10.7708 12.3368 10.0556 13.052 9.29167C13.7673 8.52778 14.3819 7.73958 14.8958 6.92708C15.4097 6.11458 15.6666 5.34722 15.6666 4.625C15.6666 3.79166 15.3888 3.09722 14.8333 2.54166C14.2777 1.98611 13.5833 1.70833 12.75 1.70833C12.0972 1.70833 11.5416 1.85764 11.0833 2.15625C10.625 2.45486 10.1944 2.88889 9.79163 3.45833C9.6944 3.59722 9.57635 3.70139 9.43746 3.77083C9.29857 3.84028 9.15274 3.875 8.99996 3.875C8.84718 3.875 8.70135 3.84028 8.56246 3.77083C8.42357 3.70139 8.30552 3.59722 8.20829 3.45833Z" fill="#09111F"/>
</Svg>

      			</View>
      			<View style={styles.text}>
        				<Text style={styles.title}>
          					{`Wishlist`}
        				</Text>
      			</View>
      			<View style={styles.itemright}>
        				<View style={styles.badgewrapper}>
          					{/* Vigma RN:: can be replaced with <Badge type={"text"} size={"small"} /> */}
          					<View style={styles.badge}>
            						<Text style={styles.label}>
              							{`120 items`}
            						</Text>
          					</View>
        				</View>
        				{/* Vigma RN:: can be replaced with <Iconright  /> */}
        				<View style={styles.iconright}>
          					<View style={styles._boundingbox}/>
<Svg style={styles._vector} width="7" height="10" viewBox="0 0 7 10" fill="none" >
<Path d="M1.08329 9.41666C0.930515 9.26388 0.854126 9.06943 0.854126 8.83332C0.854126 8.59721 0.930515 8.40277 1.08329 8.24999L4.33329 4.99999L1.08329 1.74999C0.930515 1.59721 0.854126 1.40277 0.854126 1.16666C0.854126 0.930545 0.930515 0.736101 1.08329 0.583323C1.23607 0.430545 1.43051 0.354156 1.66663 0.354156C1.90274 0.354156 2.09718 0.430545 2.24996 0.583323L6.08329 4.41666C6.16663 4.49999 6.22565 4.59027 6.26038 4.68749C6.2951 4.78471 6.31246 4.88888 6.31246 4.99999C6.31246 5.1111 6.2951 5.21527 6.26038 5.31249C6.22565 5.40971 6.16663 5.49999 6.08329 5.58332L2.24996 9.41666C2.09718 9.56943 1.90274 9.64582 1.66663 9.64582C1.43051 9.64582 1.23607 9.56943 1.08329 9.41666Z" fill="#09111F"/>
</Svg>

        				</View>
      			</View>
    		</View>
    )
}

const styles = StyleSheet.create({
  	wishlist: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingLeft: 24,
    paddingRight: 20,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 8,
    paddingVertical: 10
},
  	iconsOutlineFavorite: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 2,
    bottom: 2,
    left: 2,
    overflow: "visible"
},
  	text: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 0
},
  	title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	itemright: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 0
},
  	badgewrapper: {
    flexShrink: 0,
    paddingLeft: 0,
    paddingRight: 4,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 0,
    paddingVertical: 0
},
  	badge: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    borderRadius: 4
},
  	label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	iconright: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	_boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	_vector: {
    position: "absolute",
    flexShrink: 0,
    top: 5,
    right: 7,
    bottom: 5,
    left: 8,
    overflow: "visible"
}
})