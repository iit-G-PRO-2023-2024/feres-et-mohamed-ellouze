import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Svg, Path } from 'react-native-svg';

export default function About() {
    return (
    		<View style={styles.about}>
      			{/* Vigma RN:: can be replaced with <Iconleft  /> */}
      			<View style={styles.iconleft}>
        				<View style={styles.boundingbox}/>
<Svg style={styles.vector} width="18" height="18" viewBox="0 0 18 18" fill="none" >
<Path d="M8.99996 13.1667C9.23607 13.1667 9.43399 13.0868 9.59371 12.9271C9.75343 12.7674 9.83329 12.5694 9.83329 12.3333V8.99999C9.83329 8.76388 9.75343 8.56596 9.59371 8.40624C9.43399 8.24652 9.23607 8.16666 8.99996 8.16666C8.76385 8.16666 8.56593 8.24652 8.40621 8.40624C8.24649 8.56596 8.16663 8.76388 8.16663 8.99999V12.3333C8.16663 12.5694 8.24649 12.7674 8.40621 12.9271C8.56593 13.0868 8.76385 13.1667 8.99996 13.1667ZM8.99996 6.49999C9.23607 6.49999 9.43399 6.42013 9.59371 6.26041C9.75343 6.10068 9.83329 5.90277 9.83329 5.66666C9.83329 5.43055 9.75343 5.23263 9.59371 5.07291C9.43399 4.91318 9.23607 4.83332 8.99996 4.83332C8.76385 4.83332 8.56593 4.91318 8.40621 5.07291C8.24649 5.23263 8.16663 5.43055 8.16663 5.66666C8.16663 5.90277 8.24649 6.10068 8.40621 6.26041C8.56593 6.42013 8.76385 6.49999 8.99996 6.49999ZM8.99996 17.3333C7.84718 17.3333 6.76385 17.1146 5.74996 16.6771C4.73607 16.2396 3.85413 15.6458 3.10413 14.8958C2.35413 14.1458 1.76038 13.2639 1.32288 12.25C0.885376 11.2361 0.666626 10.1528 0.666626 8.99999C0.666626 7.84721 0.885376 6.76388 1.32288 5.74999C1.76038 4.7361 2.35413 3.85416 3.10413 3.10416C3.85413 2.35416 4.73607 1.76041 5.74996 1.32291C6.76385 0.885406 7.84718 0.666656 8.99996 0.666656C10.1527 0.666656 11.2361 0.885406 12.25 1.32291C13.2638 1.76041 14.1458 2.35416 14.8958 3.10416C15.6458 3.85416 16.2395 4.7361 16.677 5.74999C17.1145 6.76388 17.3333 7.84721 17.3333 8.99999C17.3333 10.1528 17.1145 11.2361 16.677 12.25C16.2395 13.2639 15.6458 14.1458 14.8958 14.8958C14.1458 15.6458 13.2638 16.2396 12.25 16.6771C11.2361 17.1146 10.1527 17.3333 8.99996 17.3333ZM8.99996 15.6667C10.8611 15.6667 12.4375 15.0208 13.7291 13.7292C15.0208 12.4375 15.6666 10.8611 15.6666 8.99999C15.6666 7.13888 15.0208 5.56249 13.7291 4.27082C12.4375 2.97916 10.8611 2.33332 8.99996 2.33332C7.13885 2.33332 5.56246 2.97916 4.27079 4.27082C2.97913 5.56249 2.33329 7.13888 2.33329 8.99999C2.33329 10.8611 2.97913 12.4375 4.27079 13.7292C5.56246 15.0208 7.13885 15.6667 8.99996 15.6667Z" fill="#09111F"/>
</Svg>

      			</View>
      			<View style={styles.text}>
        				<Text style={styles.title}>
          					{`About`}
        				</Text>
      			</View>
      			<View style={styles.itemright}>
        				{/* Vigma RN:: can be replaced with <Iconright  /> */}
        				<View style={styles.iconright}>
          					<View style={styles._boundingbox}/>
<Svg style={styles._vector} width="7" height="10" viewBox="0 0 7 10" fill="none" >
<Path d="M1.08329 9.41666C0.930515 9.26388 0.854126 9.06943 0.854126 8.83332C0.854126 8.59721 0.930515 8.40277 1.08329 8.24999L4.33329 4.99999L1.08329 1.74999C0.930515 1.59721 0.854126 1.40277 0.854126 1.16666C0.854126 0.930545 0.930515 0.736101 1.08329 0.583323C1.23607 0.430545 1.43051 0.354156 1.66663 0.354156C1.90274 0.354156 2.09718 0.430545 2.24996 0.583323L6.08329 4.41666C6.16663 4.49999 6.22565 4.59027 6.26038 4.68749C6.2951 4.78471 6.31246 4.88888 6.31246 4.99999C6.31246 5.1111 6.2951 5.21527 6.26038 5.31249C6.22565 5.40971 6.16663 5.49999 6.08329 5.58332L2.24996 9.41666C2.09718 9.56943 1.90274 9.64582 1.66663 9.64582C1.43051 9.64582 1.23607 9.56943 1.08329 9.41666Z" fill="#09111F"/>
</Svg>

        				</View>
      			</View>
    		</View>
    )
}

const styles = StyleSheet.create({
  	about: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingLeft: 24,
    paddingRight: 20,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 8,
    paddingVertical: 10
},
  	iconleft: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 2,
    bottom: 2,
    left: 2,
    overflow: "visible"
},
  	text: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 0
},
  	title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	itemright: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 0
},
  	iconright: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	_boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	_vector: {
    position: "absolute",
    flexShrink: 0,
    top: 5,
    right: 7,
    bottom: 5,
    left: 8,
    overflow: "visible"
}
})