import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Svg, Line } from 'react-native-svg';

export default function Divider() {
    return (
    		<View style={styles.divider}>
      			<View style={styles.linewrapper}>
<Svg style={styles.line} width="156" height="1" viewBox="0 0 156 1" fill="none" >
<Line y1="0.5" x2="156" y2="0.5" stroke="#E1E5EB"/>
</Svg>

      			</View>
      			<View style={styles._linewrapper}>
<Svg style={styles._line} width="156" height="1" viewBox="0 0 156 1" fill="none" >
<Line y1="0.5" x2="156" y2="0.5" stroke="#E1E5EB"/>
</Svg>

      			</View>
    		</View>
    )
}

const styles = StyleSheet.create({
  	divider: {
    flexShrink: 0,
    width: 360,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 24
},
  	linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 0
},
  	line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	_linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 0
},
  	_line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
}
})