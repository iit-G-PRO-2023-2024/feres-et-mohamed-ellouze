import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Svg, Path } from 'react-native-svg';

export default function Reviews() {
    return (
    		<View style={styles.reviews}>
      			{/* Vigma RN:: can be replaced with <Icon  /> */}
      			<View style={styles.icon}>
        				<View style={styles.boundingbox}/>
<Svg style={styles.vector} width="16" height="15" viewBox="0 0 16 15" fill="none" >
<Path d="M5.27217 12.1845L8.05 10.509L10.8278 12.2066L10.1003 9.03191L12.5474 6.91546L9.32868 6.62886L8.05 3.63057L6.77132 6.60682L3.55256 6.89342L5.9997 9.03191L5.27217 12.1845ZM8.05 12.5814L4.39032 14.786C4.22865 14.8889 4.05962 14.933 3.88325 14.9183C3.70688 14.9036 3.55256 14.8448 3.42028 14.7419C3.28801 14.639 3.18512 14.5104 3.11164 14.3561C3.03815 14.2018 3.02345 14.0291 3.06754 13.838L4.03758 9.67125L0.796777 6.87137C0.649802 6.73909 0.557942 6.58844 0.521198 6.41942C0.484455 6.2504 0.495478 6.08505 0.554268 5.92338C0.613058 5.76171 0.701243 5.62943 0.818823 5.52655C0.936403 5.42367 1.09808 5.35753 1.30384 5.32813L5.58082 4.95335L7.23429 1.02911C7.30778 0.85274 7.42168 0.720463 7.57601 0.632278C7.73033 0.544093 7.88833 0.5 8.05 0.5C8.21167 0.5 8.36967 0.544093 8.524 0.632278C8.67832 0.720463 8.79222 0.85274 8.86571 1.02911L10.5192 4.95335L14.7962 5.32813C15.0019 5.35753 15.1636 5.42367 15.2812 5.52655C15.3988 5.62943 15.4869 5.76171 15.5457 5.92338C15.6045 6.08505 15.6155 6.2504 15.5788 6.41942C15.5421 6.58844 15.4502 6.73909 15.3032 6.87137L12.0624 9.67125L13.0325 13.838C13.0766 14.0291 13.0619 14.2018 12.9884 14.3561C12.9149 14.5104 12.812 14.639 12.6797 14.7419C12.5474 14.8448 12.3931 14.9036 12.2167 14.9183C12.0404 14.933 11.8714 14.8889 11.7097 14.786L8.05 12.5814Z" fill="#09111F"/>
</Svg>

      			</View>
      			<View style={styles.text}>
        				<Text style={styles.title}>
          					{`Reviews`}
        				</Text>
      			</View>
      			<View style={styles.itemright}>
        				{/* Vigma RN:: can be replaced with <Iconright  /> */}
        				<View style={styles.iconright}>
          					<View style={styles._boundingbox}/>
<Svg style={styles._vector} width="7" height="10" viewBox="0 0 7 10" fill="none" >
<Path d="M1.08329 9.41666C0.930515 9.26388 0.854126 9.06943 0.854126 8.83332C0.854126 8.59721 0.930515 8.40277 1.08329 8.24999L4.33329 4.99999L1.08329 1.74999C0.930515 1.59721 0.854126 1.40277 0.854126 1.16666C0.854126 0.930545 0.930515 0.736101 1.08329 0.583323C1.23607 0.430545 1.43051 0.354156 1.66663 0.354156C1.90274 0.354156 2.09718 0.430545 2.24996 0.583323L6.08329 4.41666C6.16663 4.49999 6.22565 4.59027 6.26038 4.68749C6.2951 4.78471 6.31246 4.88888 6.31246 4.99999C6.31246 5.1111 6.2951 5.21527 6.26038 5.31249C6.22565 5.40971 6.16663 5.49999 6.08329 5.58332L2.24996 9.41666C2.09718 9.56943 1.90274 9.64582 1.66663 9.64582C1.43051 9.64582 1.23607 9.56943 1.08329 9.41666Z" fill="#09111F"/>
</Svg>

        				</View>
      			</View>
    		</View>
    )
}

const styles = StyleSheet.create({
  	reviews: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingLeft: 24,
    paddingRight: 20,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 8,
    paddingVertical: 10
},
  	icon: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 2,
    bottom: 3,
    left: 3,
    overflow: "visible"
},
  	text: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 0
},
  	title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	itemright: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 0
},
  	iconright: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	_boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	_vector: {
    position: "absolute",
    flexShrink: 0,
    top: 5,
    right: 7,
    bottom: 5,
    left: 8,
    overflow: "visible"
}
})