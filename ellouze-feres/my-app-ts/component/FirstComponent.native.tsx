import { Button,StyleSheet, Text, View, Pressable, Alert } from 'react-native'
import React, {useState,useEffect, useMemo} from 'react'

const FirstComponent = () => {

    const [count , setCount]= useState(0)
    const [start , setStart]= useState(false)

    const [price , setPrice ] = useState(1000)
    const [promotion , setPromotion ] = useState(0)
    const [priceFinal , setPriceFinal ] = useState(0)


    const priceFinalMemo = useMemo(()=>{
      return price * ((100 - promotion) / 100)
    }, [price, promotion])
    const incrument = () =>{
        setPromotion((oldValue)=>oldValue+10)
    }
    const startButton=()=>{
        setStart(!start)
    }
    let intervall : any= null
    useEffect(()=>{
        console.log("init")
    },[])
    useEffect(()=>{
        console.log("use effect", count)
    })

    useEffect(()=>{
        console.log("use effect start ", start)
        if (start)
        intervall = setInterval(()=>{
            setCount(oldValue=>oldValue+1)
        },1000)
        return()=>{
            clearInterval(intervall)
        }
    },[start])


    const calculPriceFinal = () => {

      setPriceFinal(price * ((100 - promotion) / 100))
    }

  return (
    <View>

      <Text>FirstComponent ---- 2, count: {count}</Text>
      <Text>
      price : {price}
      </Text>
      <Text>
      promotion : {promotion}
      </Text>
      <Text>
      priceFinalMemo : {priceFinalMemo}
      </Text>

      <Text>
      priceFinal : {priceFinal}
      </Text>
        <Pressable style={styles.button} >
        <Button
        title="incrument promotion"
        onPress={incrument}/>

        <Button
        title="start"
        onPress={startButton}/>
            <Button
        title="calcul"
        onPress={calculPriceFinal}/>
        </Pressable>

    </View>
    
  )
}

export default FirstComponent

const styles = StyleSheet.create({
    button: {
      alignItems: 'center',
      justifyContent: 'center',
      paddingVertical: 12,
      paddingHorizontal: 32,
      borderRadius: 4,
      elevation: 3,
      backgroundColor: 'black',
    },
    text: {
      fontSize: 16,
      lineHeight: 21,
      fontWeight: 'bold',
      letterSpacing: 0.25,
      color: 'white',
    },
  });
