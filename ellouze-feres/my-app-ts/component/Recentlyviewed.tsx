import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Svg, Path } from 'react-native-svg';

export default function Recentlyviewed() {
    return (
    		<View style={styles.recentlyviewed}>
      			{/* Vigma RN:: can be replaced with <Icon  /> */}
      			<View style={styles.icon}>
        				<View style={styles.boundingbox}/>
<Svg style={styles.vector} width="16" height="16" viewBox="0 0 16 16" fill="none" >
<Path d="M8 15.5C6.25 15.5 4.70139 14.9687 3.35417 13.9062C2.00694 12.8437 1.13194 11.4861 0.729167 9.83333C0.673611 9.625 0.715278 9.43403 0.854167 9.26042C0.993056 9.08681 1.18056 8.98611 1.41667 8.95833C1.63889 8.93056 1.84028 8.97222 2.02083 9.08333C2.20139 9.19444 2.32639 9.36111 2.39583 9.58333C2.72917 10.8333 3.41667 11.8542 4.45833 12.6458C5.5 13.4375 6.68056 13.8333 8 13.8333C9.625 13.8333 11.0035 13.2674 12.1354 12.1354C13.2674 11.0035 13.8333 9.625 13.8333 8C13.8333 6.375 13.2674 4.99653 12.1354 3.86458C11.0035 2.73264 9.625 2.16667 8 2.16667C7.04167 2.16667 6.14583 2.38889 5.3125 2.83333C4.47917 3.27778 3.77778 3.88889 3.20833 4.66667H4.66667C4.90278 4.66667 5.10069 4.74653 5.26042 4.90625C5.42014 5.06597 5.5 5.26389 5.5 5.5C5.5 5.73611 5.42014 5.93403 5.26042 6.09375C5.10069 6.25347 4.90278 6.33333 4.66667 6.33333H1.33333C1.09722 6.33333 0.899306 6.25347 0.739583 6.09375C0.579861 5.93403 0.5 5.73611 0.5 5.5V2.16667C0.5 1.93056 0.579861 1.73264 0.739583 1.57292C0.899306 1.41319 1.09722 1.33333 1.33333 1.33333C1.56944 1.33333 1.76736 1.41319 1.92708 1.57292C2.08681 1.73264 2.16667 1.93056 2.16667 2.16667V3.29167C2.875 2.40278 3.73958 1.71528 4.76042 1.22917C5.78125 0.743056 6.86111 0.5 8 0.5C9.04167 0.5 10.0174 0.697917 10.9271 1.09375C11.8368 1.48958 12.6285 2.02431 13.3021 2.69792C13.9757 3.37153 14.5104 4.16319 14.9062 5.07292C15.3021 5.98264 15.5 6.95833 15.5 8C15.5 9.04167 15.3021 10.0174 14.9062 10.9271C14.5104 11.8368 13.9757 12.6285 13.3021 13.3021C12.6285 13.9757 11.8368 14.5104 10.9271 14.9062C10.0174 15.3021 9.04167 15.5 8 15.5ZM8.83333 7.66667L10.9167 9.75C11.0694 9.90278 11.1458 10.0972 11.1458 10.3333C11.1458 10.5694 11.0694 10.7639 10.9167 10.9167C10.7639 11.0694 10.5694 11.1458 10.3333 11.1458C10.0972 11.1458 9.90278 11.0694 9.75 10.9167L7.41667 8.58333C7.33333 8.5 7.27083 8.40625 7.22917 8.30208C7.1875 8.19792 7.16667 8.09028 7.16667 7.97917V4.66667C7.16667 4.43056 7.24653 4.23264 7.40625 4.07292C7.56597 3.91319 7.76389 3.83333 8 3.83333C8.23611 3.83333 8.43403 3.91319 8.59375 4.07292C8.75347 4.23264 8.83333 4.43056 8.83333 4.66667V7.66667Z" fill="#09111F"/>
</Svg>

      			</View>
      			<View style={styles.text}>
        				<Text style={styles.title}>
          					{`Recently viewed`}
        				</Text>
      			</View>
      			<View style={styles.itemright}>
        				{/* Vigma RN:: can be replaced with <Iconright  /> */}
        				<View style={styles.iconright}>
          					<View style={styles._boundingbox}/>
<Svg style={styles._vector} width="7" height="10" viewBox="0 0 7 10" fill="none" >
<Path d="M1.08329 9.41666C0.930515 9.26388 0.854126 9.06943 0.854126 8.83332C0.854126 8.59721 0.930515 8.40277 1.08329 8.24999L4.33329 4.99999L1.08329 1.74999C0.930515 1.59721 0.854126 1.40277 0.854126 1.16666C0.854126 0.930545 0.930515 0.736101 1.08329 0.583323C1.23607 0.430545 1.43051 0.354156 1.66663 0.354156C1.90274 0.354156 2.09718 0.430545 2.24996 0.583323L6.08329 4.41666C6.16663 4.49999 6.22565 4.59027 6.26038 4.68749C6.2951 4.78471 6.31246 4.88888 6.31246 4.99999C6.31246 5.1111 6.2951 5.21527 6.26038 5.31249C6.22565 5.40971 6.16663 5.49999 6.08329 5.58332L2.24996 9.41666C2.09718 9.56943 1.90274 9.64582 1.66663 9.64582C1.43051 9.64582 1.23607 9.56943 1.08329 9.41666Z" fill="#09111F"/>
</Svg>

        				</View>
      			</View>
    		</View>
    )
}

const styles = StyleSheet.create({
  	recentlyviewed: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingLeft: 24,
    paddingRight: 20,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 8,
    paddingVertical: 10
},
  	icon: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 3,
    bottom: 3,
    left: 2,
    overflow: "visible"
},
  	text: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 0
},
  	title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	itemright: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 0
},
  	iconright: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	_boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	_vector: {
    position: "absolute",
    flexShrink: 0,
    top: 5,
    right: 7,
    bottom: 5,
    left: 8,
    overflow: "visible"
}
})