import { router } from 'expo-router';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Svg, Path, Circle } from 'react-native-svg';

export default function Navtop() {
    return (
    		<View style={styles.navtop}>
      			<View style={styles.itemleftwrapper}>
        				{/* Vigma RN:: can be replaced with <Itemleft state={"default"} /> */}
        				<View style={styles.itemleft}>
          					{/* Vigma RN:: can be replaced with <IconsOutlineArrow_back  /> */}
          					<View style={styles.iconsOutlineArrow_back}>
            						<View style={styles.boundingbox}/>
<Svg style={styles.vector} width="14" height="14" viewBox="0 0 14 14" fill="none" >
<Path d="M6.06254 13.0833L0.562541 7.58333C0.479207 7.5 0.42018 7.40972 0.385457 7.3125C0.350735 7.21528 0.333374 7.11111 0.333374 7C0.333374 6.88889 0.350735 6.78472 0.385457 6.6875C0.42018 6.59028 0.479207 6.5 0.562541 6.41667L6.06254 0.916666C6.21532 0.763889 6.40629 0.684027 6.63546 0.677083C6.86462 0.670138 7.06254 0.75 7.22921 0.916666C7.39587 1.06944 7.48268 1.26042 7.48962 1.48958C7.49657 1.71875 7.41671 1.91667 7.25004 2.08333L3.16671 6.16667H12.4792C12.7153 6.16667 12.9132 6.24653 13.073 6.40625C13.2327 6.56597 13.3125 6.76389 13.3125 7C13.3125 7.23611 13.2327 7.43403 13.073 7.59375C12.9132 7.75347 12.7153 7.83333 12.4792 7.83333H3.16671L7.25004 11.9167C7.40282 12.0694 7.48268 12.2639 7.48962 12.5C7.49657 12.7361 7.41671 12.9306 7.25004 13.0833C7.09726 13.25 6.90282 13.3333 6.66671 13.3333C6.4306 13.3333 6.22921 13.25 6.06254 13.0833Z" fill="#09111F"/>
</Svg>

          					</View>
        				</View>
      			</View>
      			<Text style={styles.title}>
        				{`Profile`}
      			</Text>
            <TouchableOpacity onPress={()=>router.push("/notification")}> 
      			<View style={styles.itemrightwrapper}>
        				{/* Vigma RN:: can be replaced with <Itemright state={"default"} /> */}
        				<View style={styles.itemright}>
          					{/* Vigma RN:: can be replaced with <Icon  /> */}
          					<View style={styles.icon}>
            						<View style={styles._boundingbox}/>
<Svg style={styles._vector} width="14" height="18" viewBox="0 0 14 18" fill="none" >
<Path d="M1.16671 14.8333C0.930596 14.8333 0.73268 14.7535 0.572957 14.5937C0.413235 14.434 0.333374 14.2361 0.333374 14C0.333374 13.7639 0.413235 13.566 0.572957 13.4062C0.73268 13.2465 0.930596 13.1667 1.16671 13.1667H2.00004V7.33332C2.00004 6.18055 2.34726 5.15624 3.04171 4.26041C3.73615 3.36457 4.63893 2.77777 5.75004 2.49999V1.91666C5.75004 1.56943 5.87157 1.2743 6.11462 1.03124C6.35768 0.788184 6.65282 0.666656 7.00004 0.666656C7.34726 0.666656 7.6424 0.788184 7.88546 1.03124C8.12851 1.2743 8.25004 1.56943 8.25004 1.91666V2.49999C9.36115 2.77777 10.2639 3.36457 10.9584 4.26041C11.6528 5.15624 12 6.18055 12 7.33332V13.1667H12.8334C13.0695 13.1667 13.2674 13.2465 13.4271 13.4062C13.5868 13.566 13.6667 13.7639 13.6667 14C13.6667 14.2361 13.5868 14.434 13.4271 14.5937C13.2674 14.7535 13.0695 14.8333 12.8334 14.8333H1.16671ZM7.00004 17.3333C6.54171 17.3333 6.14935 17.1701 5.82296 16.8437C5.49657 16.5174 5.33337 16.125 5.33337 15.6667H8.66671C8.66671 16.125 8.50351 16.5174 8.17712 16.8437C7.85074 17.1701 7.45837 17.3333 7.00004 17.3333ZM3.66671 13.1667H10.3334V7.33332C10.3334 6.41666 10.007 5.63193 9.35421 4.97916C8.70143 4.32638 7.91671 3.99999 7.00004 3.99999C6.08337 3.99999 5.29865 4.32638 4.64587 4.97916C3.9931 5.63193 3.66671 6.41666 3.66671 7.33332V13.1667Z" fill="#09111F"/>
</Svg>

          					</View>
          					{/* Vigma RN:: can be replaced with <Badge type={"dot"} size={"small"} /> */}
          					<View style={styles.badge}>
<Svg style={styles.dot} width="6" height="6" viewBox="0 0 6 6" fill="none" >
<Circle cx="3" cy="3" r="3" fill="#E50D24"/>
</Svg>

          					</View>
        				</View>
      			</View>
            </TouchableOpacity>
    		</View>
    )
}

const styles = StyleSheet.create({
  	navtop: {
    alignSelf: "stretch",
    flexShrink: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    columnGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 14
},
  	itemleftwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0
},
  	itemleft: {
    flexShrink: 0,
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999
},
  	iconsOutlineArrow_back: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	vector: {
    position: "absolute",
    flexShrink: 0,
    top: 4,
    right: 4,
    bottom: 4,
    left: 3,
    overflow: "visible"
},
  	title: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24
},
  	itemrightwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0
},
  	itemright: {
    flexShrink: 0,
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999
},
  	icon: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	_boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	_vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 3,
    bottom: 2,
    left: 3,
    overflow: "visible"
},
  	badge: {
    position: "absolute",
    flexShrink: 0,
    top: 7,
    height: 6,
    left: 27,
    width: 6,
    alignItems: "flex-start",
    rowGap: 0
},
  	dot: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    overflow: "visible"
}
})