import React from 'react';
import { View, ImageBackground, Text, StyleSheet } from 'react-native';
import {  } from 'react-native-svg';

export default function Profile() {
    return (
    		<View style={styles.profile}>
      			<ImageBackground style={styles.avatar} source={{uri: /* dummy image */ 'https://dummyimage.com/80x80/000/fff.jpg'}}/>
      			<View style={styles.text}>
        				<Text style={styles.ronaldRichards}>
          					{`Ronald Richards`}
        				</Text>
        				<Text style={styles.loremipsumdolorsitametconsecteturVivamuscongueodiorisustinciduntsed}>
          					{`Lorem ipsum dolor sit amet consectetur. Vivamus congue odio risus tincidunt sed.`}
        				</Text>
      			</View>
      			{/* Vigma RN:: can be replaced with <Buttoneditprofile type={"outline"} color={"dark"} size={"small"} state={"default"} /> */}
      			<View style={styles.buttoneditprofile}>
        				<View style={styles.textwrapper}>
          					<Text style={styles._text}>
            						{`Edit profile`}
          					</Text>
        				</View>
      			</View>
    		</View>
    )
}

const styles = StyleSheet.create({
  	profile: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "center",
    rowGap: 12,
    paddingVertical: 12,
    paddingHorizontal: 24
},
  	avatar: {
    flexShrink: 0,
    width: 80,
    height: 80,
    borderRadius: 9999
},
  	text: {
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 8
},
  	ronaldRichards: {
    flexShrink: 0,
    width: 312,
    textAlign: "center",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 20,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 28
},
  	loremipsumdolorsitametconsecteturVivamuscongueodiorisustinciduntsed: {
    flexShrink: 0,
    width: 312,
    textAlign: "center",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	buttoneditprofile: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderWidth: 1,
    borderColor: "rgba(225, 229, 235, 1)",
    borderRadius: 8
},
  	textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
},
  	_text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
}
})