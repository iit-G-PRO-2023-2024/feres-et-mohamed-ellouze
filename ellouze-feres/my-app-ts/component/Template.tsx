import { StyleSheet, Text, View,Image, ImageBackground } from 'react-native'
import React from 'react'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import Icon from 'react-native-vector-icons/FontAwesome'; // Remplacez 'FontAwesome' par le nom de l'icône que vous souhaitez utiliser
 

export default function Template() {
   return (
    <View style={styles.card}>
        <ImageBackground
        source={require('../assets/image.png')}
        style={styles.image}
      >
        <View style={styles.iconContainer}>
        <View style={styles.iconBorder}>
          <Icon name="heart" size={24} color="white" />
          </View>
        </View>
      </ImageBackground>
      
      <Text style={styles.text}>Titre </Text>
      <Text style={styles.description}>Rp 500.000</Text>
     <Text style={styles.strikethrough}>Rp 1.000.000   <Text style={styles.greenText}>50%</Text> </Text> 
      <Icon name="star" size={16} color="yellow" > <Text  style={styles.description}>  5.0</Text> </Icon>{/* Icône d'étoile */}

    </View>
  );
};

const styles = StyleSheet.create({
    card: {
      borderRadius: 10,
      backgroundColor: 'white',
      shadowColor: 'black',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.3,
      shadowRadius: 2,
      elevation: 3,
      margin: 10,
    },
    iconBorder: {
        borderWidth: 1, // Ajoute une bordure noire
        borderColor: 'black',
        borderRadius: 50, // Arrondit le conteneur pour obtenir une bordure circulaire
        padding: 5, // Ajustez le padding pour contrôler la taille de la bordure
      },
    iconContainer: {
        position: 'absolute',
        top: 10,
        right: 10,
      },
    image: {
      width: 550,
      height: 1004,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
    },
    text: {
       
      fontSize: 16,
      fontWeight: 'bold', 
    },
    description: { 
        color:'black',
        fontSize: 14, 
        fontWeight: 'bold', 
      },
      strikethrough: { 
        textDecorationLine: 'line-through', // Applique une ligne barrée
      },
      greenText: {
        color: 'green', // Applique la couleur verte
      },
  });
  
 