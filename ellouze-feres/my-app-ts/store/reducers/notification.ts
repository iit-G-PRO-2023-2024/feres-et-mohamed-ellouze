 
const notifications: Notification[] = [];
const initialState = {
  notifications,
};

const notificationReducer = (state = initialState, action:any) => {
  switch (action.type) {
    case "SET_NOTIFICATIONS":
      return {
        ...state,
        notifications: action.payload,
      };
    default:
      return state;
  }
};

export default notificationReducer;
