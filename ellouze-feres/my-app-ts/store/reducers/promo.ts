type Promo = {
  id: string;
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
  title?: string;
  description?: string;
  typeId?: string;
  type?: object;
  colorId?: string;
  color?: object;
  price?: number;
  ratingsId?: string;
  ratings?: object;
  promotion?: number;
  image?: string;
  ordersId?: string;
  orders?: object;
}

const initialState = {
promos : []
}

const promoReducer = (state = initialState, action) => {

  switch(action.type){
      case "SET_PROMOS":
          console.log({
              ...state, 
              promos : action.payload
              
          })
          return {
              ...state, 
              promos : action.payload
              
          }
  }

return state


}

export default promoReducer;